package milkFactory.presenter;

import milkFactory.model.Product;
import milkFactory.model.type.ProductType;
import milkFactory.view.FactoryView;

import java.util.*;

public class ViewPresenter {

    private FactoryView factoryView;

    private List<Product> listOfProduct = new ArrayList<>();

    public ViewPresenter(FactoryView factoryView) {
        this.factoryView = factoryView;
        initListOfProduct();
        sortCollectionByType();
    }

    public void startFactoryWork() {
        factoryView.startFactoryWork();
        do {
            factoryView.variantOfFactoryWork();
        } while (true);
    }

    private void initListOfProduct() {
        listOfProduct.add(new Product(1, 0.5, ProductType.MILK));
        listOfProduct.add(new Product(2, 1, ProductType.YOGURT));
        listOfProduct.add(new Product(3, 0.3, ProductType.CREAM));
        listOfProduct.add(new Product(4, 2, ProductType.YOGURT));
        listOfProduct.add(new Product(5, 1, ProductType.MILK));
        listOfProduct.add(new Product(6, 0.5, ProductType.CREAM));
        listOfProduct.add(new Product(7, 0.2, ProductType.SOURCREAM));
        listOfProduct.add(new Product(8, 0.7, ProductType.CREAM));
        listOfProduct.add(new Product(9, 0.4, ProductType.SOURCREAM));
        listOfProduct.add(new Product(10, 2, ProductType.MILK));
    }

    public void createNewProduct() {
        Product newProduct = factoryView.createProduct();
        if (!checkProductById(newProduct.getId())) {
            listOfProduct.add(newProduct);
        } else factoryView.showData("Product with ID " + newProduct.getId() + " already in the list");
    }

    private boolean checkProductById(int id) {
        for (Product item : listOfProduct) {
            if (item.getId() == id) {
                return true;
            }
        }
        return false;
    }


    public void showProductById() {
        Product exampleProduct = new Product();

        factoryView.showData("Enter ID: ");
        int exampleProductId = Integer.parseInt(factoryView.getDataFromConsole());
        exampleProduct.setId(exampleProductId);

        for (Product item : listOfProduct) {
            if (item.getId() == exampleProductId) {
                factoryView.showProduct(item);
            }
        }

    }

    public void showAllProducts() {
        for (Product item : listOfProduct) {
            factoryView.showProduct(item);
            factoryView.showSeparator();
        }
    }


    public void deleteProductById(int id) {
        Product deletedProduct = null;
        for (Product item : listOfProduct) {
            if (item.getId() == id) {
                deletedProduct = item;
            }
        }

        if (deletedProduct == null) {
            factoryView.showData("Product with ID " + id + " not found");
            return;
        }

        listOfProduct.remove(deletedProduct);

    }

    public void sortCollectionById() {
        Collections.sort(listOfProduct, Comparator.comparing(Product::getId));
//        listOfProduct.sort(Comparator.comparing(Product::getId));
    }

    public void sortCollectionByType(){
        Collections.sort(listOfProduct, Comparator.comparing(Product::getProductType));
    }



}
