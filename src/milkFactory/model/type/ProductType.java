package milkFactory.model.type;

public enum ProductType {
    MILK,
    YOGURT,
    SOURCREAM,
    CREAM
}
