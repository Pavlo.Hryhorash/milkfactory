package milkFactory.model;

import milkFactory.model.type.ProductType;

public class Product {

    private int id;
    private double capacity;
    private ProductType productType;

    public Product() {
    }

    public Product(int id, double capacity, ProductType productType) {
        this.id = id;
        this.capacity = capacity;
        this.productType = productType;
    }

    public int getId() {
        return id;
    }

    public double getCapacity() {
        return capacity;
    }

    public ProductType getProductType() {
        return productType;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setCapacity(double capacity) {
        this.capacity = capacity;
    }

    public void setProductType(ProductType productType) {
        this.productType = productType;
    }
}
