package milkFactory.view;

import milkFactory.model.Product;

public interface FactoryView {
    void startFactoryWork();
    void variantOfFactoryWork();
    void showData(String message);
    void showSeparator();
    String getDataFromConsole();
    void showProduct(Product product);
    Product createProduct();

}
