package milkFactory.view;

import milkFactory.model.Product;
import milkFactory.model.type.ProductType;
import milkFactory.presenter.ViewPresenter;

import java.util.Scanner;

public class FactoryViewImpl implements FactoryView {

    private ViewPresenter mViewPresenter;

    public FactoryViewImpl() {
        mViewPresenter = new ViewPresenter(this);
        mViewPresenter.startFactoryWork();
    }

    @Override
    public void showData(String message) {
        System.out.println(message);
    }

    @Override
    public void showSeparator() {
        System.out.println("==========================================================");
    }

    @Override
    public String getDataFromConsole() {
        Scanner scanner = new Scanner(System.in);
        return scanner.next();
    }

    @Override
    public void showProduct(Product product) {
        System.out.println("id -> " + product.getId() + "\n" +
                "capacity -> " + product.getCapacity() + "\n" +
                "product type -> " + product.getProductType());
    }

    @Override
    public Product createProduct() {

        System.out.println("Enter ID: ");
        int productId = new Scanner(System.in).nextInt();

        System.out.println("Enter capacity: ");
        double productCapacity = new Scanner(System.in).nextDouble();

        System.out.println("Enter product type: 1 if milk, 2 if yogurt, 3 if cream, 4 if sour cream ");

        ProductType exampleProductType;
        boolean isProductTypeNotValid = true;
        do {
            exampleProductType = createProductType(new Scanner(System.in).nextInt());
            if ( exampleProductType != null){
                isProductTypeNotValid = false ;
            } else System.out.println("You enter wrong type");
        } while (isProductTypeNotValid);


        return new Product(productId, productCapacity, exampleProductType);

    }

    private ProductType createProductType(int index) {

        switch (index) {
            case 1:
                return ProductType.MILK;
            case 2:
                return ProductType.YOGURT;
            case 3:
                return ProductType.CREAM;
            case 4:
                return ProductType.SOURCREAM;
            default:
                return null;
        }
    }

    @Override
    public void startFactoryWork() {
        System.out.println("Welcome to factory");
    }

    @Override
    public void variantOfFactoryWork() {
        System.out.println("Choose variant of factory work: ");
        System.out.println("1 - createNewProduct ");
        System.out.println("2 - showProductById ");
        System.out.println("3 - showAllProducts ");
        System.out.println("4 - deleteProductById");
        System.out.println("5 - stop factory work");

        Scanner scanner = new Scanner(System.in);
        switch (scanner.nextInt()) {
            case 1:
                mViewPresenter.createNewProduct();
                break;
            case 2:
                mViewPresenter.showProductById();
                break;
            case 3:
                mViewPresenter.showAllProducts();
                break;
            case 4:
                System.out.println("Enter ID: ");
                int productId = scanner.nextInt();
                mViewPresenter.deleteProductById(productId);
                break;
            case 5:
                System.exit(0);
                break;
        }
    }
}
